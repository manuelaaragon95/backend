const {Schema, model}  = require('mongoose');

const ClinicasSchema = Schema({
    nombre:{
        type: String,
        required: true
    },
    img:{
        type: String,
    },
    usuario:{
        type:Schema.Types.ObjectId,
        ref:'Usuario'
    }
}, { collection: 'clinicas' });

ClinicasSchema.method('toJSON', function() {
    const {__v, ...object} = this.toObject();  
    object.uid = _id;
    return object;
})

module.exports = model( 'clinica', ClinicasSchema );